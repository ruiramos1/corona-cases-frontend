import React from "react";
import styled, { createGlobalStyle } from "styled-components";

import { QueryRenderer } from "react-relay";
import environment from "./relay-environment";

import Cases from "./components/Cases";
import Deaths from "./components/Deaths";

const graphql = require("babel-plugin-relay/macro");

const Main = styled.main`
  display: flex;
  width: 100%;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const Container = styled.div`
  flex-grow: 1;
  width: 50%;
  @media (max-width: 768px) {
    width: 100%;
  }
`;

const Title = styled.h1`
  text-align: center;
  font-weight: 500;
  margin-bottom: 0.25em;
`;

const Subtitle = styled.small`
  text-align: center;
  display: block;
  margin-bottom: 1em;
  color: #666;
`;

const GlobalStyle = createGlobalStyle`
  body {
    font-family: sans-serif;
  }
`;

const App: React.FC<{}> = () => (
  <QueryRenderer
    environment={environment}
    query={graphql`
      query AppQuery {
        ...Cases_cases
        ...Deaths_deaths
      }
    `}
    variables={{}}
    render={({ error, props }: any) => {
      if (props) {
        return (
          <>
            <GlobalStyle />
            <Title>Covid19 UK dashboard</Title>
            <Subtitle>
              Source:{" "}
              <a href="https://www.arcgis.com/home/item.html?id=23258c605db74e6696e72a65513a1770">
                Public Health England
              </a>
            </Subtitle>
            <Main>
              <Container>
                <Cases cases={props} />
              </Container>
              <Container>
                <Deaths deaths={props} />
              </Container>
            </Main>
          </>
        );
      } else {
        return <p>Loading...</p>;
      }
    }}
  />
);

export default App;
