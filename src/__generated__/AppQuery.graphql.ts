/* tslint:disable */
/* eslint-disable */
/* @relayHash e19b960795b75c6a5884c25eb05ee14e */

import { ConcreteRequest } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type AppQueryVariables = {};
export type AppQueryResponse = {
    readonly " $fragmentRefs": FragmentRefs<"Cases_cases" | "Deaths_deaths">;
};
export type AppQuery = {
    readonly response: AppQueryResponse;
    readonly variables: AppQueryVariables;
};



/*
query AppQuery {
  ...Cases_cases
  ...Deaths_deaths
}

fragment Cases_cases on Query {
  cases {
    date
    cumCases
    dailyCases
  }
}

fragment Deaths_deaths on Query {
  deaths {
    date
    cumDeaths
    dailyDeaths
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "date",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "AppQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "FragmentSpread",
        "name": "Cases_cases",
        "args": null
      },
      {
        "kind": "FragmentSpread",
        "name": "Deaths_deaths",
        "args": null
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "AppQuery",
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "cases",
        "storageKey": null,
        "args": null,
        "concreteType": "Case",
        "plural": true,
        "selections": [
          (v0/*: any*/),
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "cumCases",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "dailyCases",
            "args": null,
            "storageKey": null
          }
        ]
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "deaths",
        "storageKey": null,
        "args": null,
        "concreteType": "Death",
        "plural": true,
        "selections": [
          (v0/*: any*/),
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "cumDeaths",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "dailyDeaths",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "AppQuery",
    "id": null,
    "text": "query AppQuery {\n  ...Cases_cases\n  ...Deaths_deaths\n}\n\nfragment Cases_cases on Query {\n  cases {\n    date\n    cumCases\n    dailyCases\n  }\n}\n\nfragment Deaths_deaths on Query {\n  deaths {\n    date\n    cumDeaths\n    dailyDeaths\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = '17f8995ecd6d5c4b214a47988e6fde4b';
export default node;
