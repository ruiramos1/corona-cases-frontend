import React from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { createFragmentContainer } from "react-relay";
import Table from "./Table";

import { Cases_cases } from "./__generated__/Cases_cases.graphql";
const graphql = require("babel-plugin-relay/macro");

const Cases: React.FC<{ cases: Cases_cases }> = ({ cases: { cases } }) => {
  const dates = cases ? cases.map((el) => el && +el.date) : [];
  const lastDate = new Date(dates[dates.length - 1] || 0);

  const options = {
    title: {
      text: `Cases`,
    },
    xAxis: {
      type: "datetime",
    },
    yAxis: [
      {
        title: {
          text: "Cases",
        },
      },
      {
        title: {
          text: "",
        },
        opposite: true,
      },
    ],
    series: [
      {
        name: "Daily",
        type: "column",
        yAxis: 1,
        data: cases ? cases.map((el: any) => [+el.date, el.dailyCases]) : [],
      },
      {
        name: "Cumulative",
        type: "line",
        data: cases ? cases.map((el: any) => [+el.date, el.cumCases]) : [],
      },
    ],
  };

  return (
    <>
      <HighchartsReact highcharts={Highcharts} options={options} />
      <Table
        data={cases ? cases : []}
        cumKey="cumCases"
        dailyKey="dailyCases"
      />
    </>
  );
};

export default createFragmentContainer(Cases, {
  cases: graphql`
    fragment Cases_cases on Query {
      cases {
        date
        cumCases
        dailyCases
      }
    }
  `,
});
