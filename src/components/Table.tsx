import React, { useState } from "react";
import styled from "styled-components";

type TableProps = {
  dailyKey: string;
  cumKey: string;
  data: readonly (object | null)[];
};

const NumTd = styled.td`
  text-align: right;
`;

const NumTh = styled.th`
  text-align: right;
`;

const StyledTable = styled.table`
  width: 80%;
  margin: 0 auto;
`;

const TableContainer = styled.div`
  max-height: 250px;
  overflow: hidden;
  position: relative;

  &.expanded {
    max-height: none;
  }
`;

const StyledButton = styled.button`
  position: absolute;
  bottom: 0;
`;

const Table: React.FC<TableProps> = ({ data, cumKey, dailyKey }) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <TableContainer className={expanded ? "expanded" : ""}>
      <StyledTable>
        <thead>
          <tr>
            <th>Date</th>
            <NumTh>Daily</NumTh>
            <NumTh>Growth rate</NumTh>
            <NumTh>Cumulative</NumTh>
          </tr>
        </thead>
        <tbody>
          {data
            ? data
                .slice(0)
                .reverse()
                .map((el: any, i) => {
                  // todo
                  let d = new Date(+el.date);
                  let prevVal: any = data[data.length - i - 2]
                    ? (data[data.length - i - 2] as any)[dailyKey]
                    : 0;
                  return (
                    <tr key={i}>
                      <td>{`${d.getDate()}/${d.getMonth()+1}/${d.getFullYear()}
                      `}</td>
                      <NumTd>{el[dailyKey]}</NumTd>
                      <NumTd>
                        {prevVal
                          ? `${Math.round(
                              (100 * (el[dailyKey] - prevVal)) / prevVal
                            )}%`
                          : "-"}
                      </NumTd>
                      <NumTd>{el[cumKey]}</NumTd>
                    </tr>
                  );
                })
            : []}
        </tbody>
      </StyledTable>
      <StyledButton onClick={() => setExpanded(!expanded)}>Expand</StyledButton>
    </TableContainer>
  );
};

export default Table;
