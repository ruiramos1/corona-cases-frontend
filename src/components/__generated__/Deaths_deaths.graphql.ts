/* tslint:disable */
/* eslint-disable */

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type Deaths_deaths = {
    readonly deaths: ReadonlyArray<{
        readonly date: string;
        readonly cumDeaths: number | null;
        readonly dailyDeaths: number | null;
    } | null> | null;
    readonly " $refType": "Deaths_deaths";
};
export type Deaths_deaths$data = Deaths_deaths;
export type Deaths_deaths$key = {
    readonly " $data"?: Deaths_deaths$data;
    readonly " $fragmentRefs": FragmentRefs<"Deaths_deaths">;
};



const node: ReaderFragment = {
  "kind": "Fragment",
  "name": "Deaths_deaths",
  "type": "Query",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "deaths",
      "storageKey": null,
      "args": null,
      "concreteType": "Death",
      "plural": true,
      "selections": [
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "date",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "cumDeaths",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "dailyDeaths",
          "args": null,
          "storageKey": null
        }
      ]
    }
  ]
};
(node as any).hash = 'a793f45594e35cd8a04e2526edbc6a9f';
export default node;
