/* tslint:disable */
/* eslint-disable */

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type Cases_cases = {
    readonly cases: ReadonlyArray<{
        readonly date: string;
        readonly cumCases: number | null;
        readonly dailyCases: number | null;
    } | null> | null;
    readonly " $refType": "Cases_cases";
};
export type Cases_cases$data = Cases_cases;
export type Cases_cases$key = {
    readonly " $data"?: Cases_cases$data;
    readonly " $fragmentRefs": FragmentRefs<"Cases_cases">;
};



const node: ReaderFragment = {
  "kind": "Fragment",
  "name": "Cases_cases",
  "type": "Query",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "cases",
      "storageKey": null,
      "args": null,
      "concreteType": "Case",
      "plural": true,
      "selections": [
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "date",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "cumCases",
          "args": null,
          "storageKey": null
        },
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "dailyCases",
          "args": null,
          "storageKey": null
        }
      ]
    }
  ]
};
(node as any).hash = '9fbb1d386b3eb8dced3fc3f6ffc9d6ea';
export default node;
