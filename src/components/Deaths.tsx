import React from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { createFragmentContainer } from "react-relay";
import Table from "./Table";

import { Deaths_deaths } from "./__generated__/Deaths_deaths.graphql";
const graphql = require("babel-plugin-relay/macro");

const Deaths: React.FC<{ deaths: Deaths_deaths }> = ({
  deaths: { deaths },
}) => {
  const dates = deaths ? deaths.map((el) => el && +el.date) : [];
  const lastDate = new Date(dates[dates.length - 1] || 0);

  const options = {
    title: {
      text: `Deaths`,
    },
    xAxis: {
      type: "datetime",
    },
    yAxis: [
      {
        title: {
          text: "Deaths",
        },
      },
      {
        title: {
          text: "",
        },
        opposite: true,
      },
    ],
    series: [
      {
        name: "Daily",
        type: "column",
        yAxis: 1,
        data: deaths ? deaths.map((el: any) => [+el.date, el.dailyDeaths]) : [],
      },
      {
        name: "Cumulative",
        type: "line",
        data: deaths ? deaths.map((el: any) => [+el.date, el.cumDeaths]) : [],
      },
    ],
  };

  return (
    <>
      <HighchartsReact highcharts={Highcharts} options={options} />{" "}
      <Table
        data={deaths ? deaths : []}
        cumKey="cumDeaths"
        dailyKey="dailyDeaths"
      />
    </>
  );
};

export default createFragmentContainer(Deaths, {
  deaths: graphql`
    fragment Deaths_deaths on Query {
      deaths {
        date
        cumDeaths
        dailyDeaths
      }
    }
  `,
});
